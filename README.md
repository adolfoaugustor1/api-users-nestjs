# NestTeste

An API for Users

## Start Docker

```bash
   docker-compose up -d
```

## Install Dependencies

```bash
   $ npm i
```

## Initialize server

```bash
   npm run start:dev
```

## Running the app

```bash
    # development
    $ npm run start

    # watch mode
    $ npm run start:dev

    # production mode
    $ npm run start:prod
```

## Url Swagger for Api Documentation
```
http://127.0.0.1:3000/api/doc
```
