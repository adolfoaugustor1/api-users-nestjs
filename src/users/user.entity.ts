import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, type: 'varchar', length: 200 })
  name: string;
  
  @CreateDateColumn()
  birthdate: Date;

  @Column({ nullable: true, type: 'varchar', length: 200 })
  document: string;

  @Column({ nullable: false, default: false })
  acceptedTerms: boolean;

  @Column({ nullable: false })
  zipcode: number;

  @Column({ nullable: true, type: 'varchar', length: 200 })
  street: string;

  @Column({ nullable: true, type: 'varchar', length: 200 })
  neighborhood: string;

  @Column({ nullable: true, type: 'varchar', length: 200 })
  city: string;

  @Column({ nullable: true, type: 'varchar', length: 200 })
  state: string;
  
  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}