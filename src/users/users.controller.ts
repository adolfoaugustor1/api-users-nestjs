import { Body, Controller, Delete, Get, Param, Post, Headers, UseGuards, Patch, ValidationPipe } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { ReturnUserDto } from './dtos/return-user.dto';
import { UpdateUserDto } from './dtos/update-user.dto.ts';
import { User } from './user.entity';
import { UsersService } from './users.service';
import { ValideGuard } from './valideGuard';

@Controller('users')
export class UsersController {
   constructor(private usersService: UsersService) {}

   @Get()
   async allUsers(): Promise<User[]>{
      return await this.usersService.findAllUsers();
   }

   @Get(':id')
   async findUser(@Param('id') id: number): Promise<User>{
      return await this.usersService.findUser(id);
   }

   @Post('create')
   async createUser(
      @Body() createUserDto: CreateUserDto,
   ): Promise<ReturnUserDto> {
      
      const user = await this.usersService.createUser(createUserDto);
      return {
         user,
         message: 'usuário cadastrado com sucesso',
      };
   }

   @Patch(':id')
   async updateUser(@Body(ValidationPipe) updateUserDto: UpdateUserDto, @Param('id') id:number ): Promise<void> {
      return await this.usersService.updateUser(updateUserDto, id);
   }

   @Delete(':id')
   @UseGuards(ValideGuard)
   async deleteUser(@Param('id') id: number) {
      
      await this.usersService.deleteUser(id);
      return {
         message: 'Usuário removido com sucesso',
      }
   }

   @Get('search/:name')
   async getUserByName(@Param('name') name: string) {
      return await this.usersService.searchUser(name);
   }
}
