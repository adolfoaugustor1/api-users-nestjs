import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { User } from './user.entity';
import { UserRepository } from './user.repository';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private httpService: HttpService
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const newObj = Object();
    const { zipcode, birthdate } = createUserDto;

    if(this.verifyAge(birthdate)){
      throw new NotFoundException("Usuário não pode ser cadastrado, é menor de idade!")
    }

    const { data } = await this.getCep(zipcode);
    
    newObj.name = createUserDto.name;
    newObj.birthdate = createUserDto.birthdate;
    newObj.document = createUserDto.document;
    newObj.acceptedTerms = createUserDto.acceptedTerms;
    newObj.zipcode = createUserDto.zipcode;
    newObj.street = data.logradouro;
    newObj.neighborhood = data.bairro;
    newObj.city = data.localidade;
    newObj.state = data.uf;
    
    return await this.userRepository.createUser(newObj);
  }

  async updateUser(updateUserDto, id): Promise<void>{
    return await this.userRepository.updateUser(updateUserDto, id);
  }

  async deleteUser(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);
    
    if(user == null){
      throw new NotFoundException("Usuário não encontrado ou já excluído.");
    }

    return this.userRepository.remove(user);
  }

  async findAllUsers(): Promise<User[]> {
    const users = await this.userRepository.find();
    if(users.length == 0){
      throw new NotFoundException("Não contém usuários.");
    }
    return users;
  }

  async findUser(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);
    if(!user){
      throw new NotFoundException("Usuário não encontrado.");
    }
    return user;
  }
  
  async searchUser(name: string) {
    const search = await this.userRepository.find({name: name});
    if(!search){
      throw new NotFoundException("Usuário não encontrado.");
    }

    return search;
  }

  async getCep(zipcode): Promise<AxiosResponse>{
    return this.httpService.get(`https://viacep.com.br/ws/${ zipcode }/json/`).toPromise();
  }

  private verifyAge(date){
    const birthdate = new Date(date).getFullYear();
    const today = new Date().getFullYear();
    
    return (today - birthdate) <= 17;
  }
}