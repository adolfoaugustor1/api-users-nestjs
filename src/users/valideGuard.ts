import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

@Injectable()
export class ValideGuard implements CanActivate {
   canActivate(
      context: ExecutionContext,
   ): any{
      const request = context.switchToHttp().getRequest().headers;

      return request['access-token'] == 'meegu';
   }
}
