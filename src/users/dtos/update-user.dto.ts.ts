import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength } from "class-validator";

export class UpdateUserDto {

  @IsNotEmpty({ message: 'Informe o nome do usuário.',})
  @MinLength(2, { message: 'Nome com mínimo de dois caracteres.', })
  @MaxLength(100, { message: 'O nome deve ter menos de 200 caracteres.',})
  @ApiProperty()
  name: string;

  @IsNotEmpty({ message: 'Data de nascimento obrigatório.',})
  @IsDate({ message: 'Data de nascimento obrigatório.',})
  @ApiProperty()
  birthdate: Date;
  
  @IsNumber()
  @IsNotEmpty({ message: 'Campo CEP obrigatório.',})
  @ApiProperty()
  zipcode: number;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  document: string;
  
  @IsBoolean()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  acceptedTerms: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  street: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  neighborhood: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  city: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, nullable: true })
  state: string;
}