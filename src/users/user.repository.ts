import { NotFoundException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { UpdateUserDto } from './dtos/update-user.dto.ts';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
   async createUser( data ): Promise<User> {
      const { name, birthdate, document, acceptedTerms, zipcode, street, neighborhood, city, state } = data;
  
      const user = this.create();
      user.name          = name;
      user.birthdate     = birthdate;
      user.document      = document;
      user.acceptedTerms = acceptedTerms;
      user.zipcode       = zipcode;
      user.street        = street;
      user.neighborhood  = neighborhood;
      user.city          = city;
      user.state         = state;

      try {
         return await user.save();
      } catch (error) {
         throw new NotFoundException(
            `Erro ao salvar o usuário no banco de dados: ${error}`,
         );
      }
   }

   async updateUser( updateUserDto: UpdateUserDto, id ): Promise<any> {
      const { name, birthdate, document, acceptedTerms, zipcode, street, neighborhood, city, state } = updateUserDto;
      const user = await this.findOne(id);
      
      try {
         user.name          = name ?? name;
         user.birthdate     = birthdate ?? birthdate;
         user.document      = document ?? document;
         user.acceptedTerms = acceptedTerms ?? acceptedTerms;
         user.zipcode       = zipcode ?? zipcode;
         user.street        = street ?? street;
         user.neighborhood  = neighborhood ?? neighborhood;
         user.city          = city ?? city;
         user.state         = state ?? state;

         return await user.save();
      } catch (error) {
         throw new NotFoundException(
            `Usuário não encontrado ou dados inválidos ${error}`,
         );
      }
   }
}